<?php

declare(strict_types=1);

require_once __DIR__ . '/../vendor/autoload.php';

$hello = new App\HelloWorld();

echo $hello->hello();
var_dump($hello->getUser());
