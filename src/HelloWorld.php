<?php

namespace App;

use App\Resources\UserDTO;

class HelloWorld
{
    public function hello(): string
    {
        return 'Hello World!';
    }

    public function getUser()
    {
        return UserDTO::create([
            'id' => 1,
            'name' => 'John Doe',
            'email' => 'same@example.com',
        ])->toArray();
    }
}
