<?php

declare(strict_types=1);

namespace App\Resources;

use App\Resources\Shared\Traits\StaticCreateSelf;
use App\Resources\Shared\Traits\ToArray;

class UserDTO
{
    use StaticCreateSelf;
    use ToArray;

    private int $id;
    private string $name = '';
    private string $email = '';

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}
