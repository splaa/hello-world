<?php

declare(strict_types=1);

namespace App\Resources\Shared\Traits;

trait StaticCreateSelf
{
    public static function create(array $data): self
    {
        $self = new self();
        foreach ($data as $key => $value) {
            if (property_exists($self, $key)) {
                $self->{$key} = $value;
            }
        }
        return $self;
    }
}
